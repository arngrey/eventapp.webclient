const proxy = {
  '/api/hobbies': [{
    id: 'test',
    name: 'test',
  }, {
    id: '1',
    name: '1',
  }, {
    id: '2',
    name: '2',
  }, {
    id: '3',
    name: '4',
  }, {
    id: '5',
    name: '5',
  }, {
    id: '6',
    name: '7',
  }],
  '/api/campaigns': [{
    id: '1',
    name: 'camp',
    administrator: {
      id: '1',
      login: '123',
      firstName: 'Vlad',
    },
    hobbies: [{
      id: '1',
      name: '1',
    }, {
      id: '2',
      name: '2',
    }],
    participants: [{
      id: '1',
      login: '123',
      firstName: 'Vlad',
    }],
    messages: [],
  }, {
    id: '2',
    name: 'campasdasd',
    administrator: {
      id: '2',
      login: '111',
      firstName: 'Veronica',
    },
    hobbies: [{
      id: '3',
      name: '4',
    }, {
      id: '5',
      name: '5',
    }],
    participants: [{
      id: '2',
      login: '111',
      firstName: 'Veronica',
    }],
    messages: [],
  }],
  'POST /api/users/signin': (request, response) => {
    const { password, login } = request.body;
    if (password === '123' && login === '123') {
      return response.status(200).json({ id: 1 });
    } else {
      return response.status(403);
    }
  },
  'POST /api/users/signup': (request, response) => {
    const { password, login } = request.body;
    if (password === '123' && login === '123') {
      return response.status(200).json({ id: 1 });
    } else {
      return response.status(403);
    }
  },
}
module.exports = proxy;