import React, { FC } from "react";
import Select, { SingleValue, MultiValue } from "react-select";

import { ArnLabel } from "app/components/atoms/ArnLabel";

import {
  ArnSelectFieldContainer,
  ArnSelectContainer,
  ArnLabelContainer,
} from "./styles";

export type ArnSelectFieldOption = {
  label: string;
  value: string;
};

export type ArnSelectFieldProps = {
  labelText: string;
  isMultiple?: boolean;
  options: ArnSelectFieldOption[];
  onChange: (
    newOption:
      | SingleValue<ArnSelectFieldOption>
      | MultiValue<ArnSelectFieldOption>
  ) => void;
};

export const ArnSelectField: FC<ArnSelectFieldProps> = ({
  labelText,
  isMultiple,
  options,
  onChange,
}) => (
  <ArnSelectFieldContainer>
    <ArnLabelContainer>
      <ArnLabel text={labelText} />
    </ArnLabelContainer>
    <ArnSelectContainer>
      <Select
        isMulti={isMultiple}
        styles={{ container: () => ({ width: "100%" }) }}
        onChange={(newOption) => onChange(newOption)}
        options={options}
      />
    </ArnSelectContainer>
  </ArnSelectFieldContainer>
);
