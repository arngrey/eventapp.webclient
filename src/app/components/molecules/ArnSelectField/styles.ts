import styled from "styled-components";

export const ArnSelectFieldContainer = styled.div`
  display: flex;
  position: relative;
  width: 100%;
  height: fit-content;
  flex-direction: row;
  align-items: center;
`;

export const ArnLabelContainer = styled.div`
  flex: 1;
  margin-right: .7rem;
`;

export const ArnSelectContainer = styled.div`
  display: flex;
  position: relative;
  width: 100%;
  height: fit-content;
  flex-direction: row;
  align-items: center;
  flex: 3;
`;
