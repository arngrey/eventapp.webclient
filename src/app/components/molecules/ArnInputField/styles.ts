import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  position: relative;
  width: 100%;
  flex-direction: row;
  align-items: center;
`;

export const LabelContainer = styled.div`
  flex: 1;
  margin-right: .5rem;
`;

export const InputContainer = styled.div`
  flex: 3;
`;