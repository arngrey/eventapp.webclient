import React, { ChangeEvent, FC } from "react";

import { ArnInput } from "app/components/atoms/ArnInput";
import { ArnLabel } from "app/components/atoms/ArnLabel";

import { Container, InputContainer, LabelContainer } from "./styles";

export type ArnInputFieldProps = {
  labelText: string;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
};

export const ArnInputField: FC<ArnInputFieldProps> = ({
  labelText,
  onChange,
}) => (
  <Container>
    <LabelContainer>
      <ArnLabel text={labelText} />
    </LabelContainer>
    <InputContainer>
      <ArnInput onChange={onChange} stretched />
    </InputContainer>
  </Container>
);
