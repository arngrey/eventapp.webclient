import React, { FC } from "react";

import { ArnButton, ArnButtonProps } from "app/components/atoms/ArnButton";

import { DefaultArnButtonPanel } from "./styles";

export type ArnButtonPanelProps = {
  buttons: ArnButtonProps[];
};

export const ArnButtonPanel: FC<ArnButtonPanelProps> = ({ buttons }) => {
  return (
    <DefaultArnButtonPanel>
      {buttons
        .filter((button) => button.visible === undefined || button.visible)
        .map((button, i) => (
          <ArnButton key={i} text={button.text} onClick={button.onClick} />
        ))}
    </DefaultArnButtonPanel>
  );
};
