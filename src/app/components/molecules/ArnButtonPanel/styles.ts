import styled from "styled-components";

export const DefaultArnButtonPanel = styled.div`
  display: flex;
  position: relative;
  flex-direction: row;
  & > *:not(:last-child) {
    margin-right: 1rem;
  }
`;
