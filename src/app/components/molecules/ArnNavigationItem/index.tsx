import React, { FC } from "react";
import { Link } from "react-router-dom";

import { DefaultArnNavigationItem } from "./styles";

export type ArnNavigationItemProps = {
  title: string;
  path: string;
  isVisible?: boolean;
};

export const ArnNavigationItem: FC<ArnNavigationItemProps> = ({
  title,
  path,
  isVisible,
}) => {
  return (
    <DefaultArnNavigationItem>
      <Link to={path}>{title}</Link>
    </DefaultArnNavigationItem>
  );
};
