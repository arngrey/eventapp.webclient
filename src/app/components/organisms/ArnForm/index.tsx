import React, { FC, useState } from "react";

import { ArnButtonProps } from "app/components/atoms/ArnButton";
import { ArnTitle } from "app/components/atoms/ArnTitle";
import { ArnButtonPanel } from "app/components/molecules/ArnButtonPanel";
import {
  ArnInputField,
  ArnInputFieldProps,
} from "app/components/molecules/ArnInputField";
import {
  ArnSelectField,
  ArnSelectFieldProps,
} from "app/components/molecules/ArnSelectField";

import { Container, FieldsContainer, TitleContainer } from "./styles";

type FieldRecords = Record<string, string | string[]>;

type FieldProps = {
  name: string;
};

export type SelectFieldProps = FieldProps & {
  type: "select";
  props: Omit<ArnSelectFieldProps, "onChange">;
};

export type InputFieldProps = FieldProps & {
  type: "input";
  props: Omit<ArnInputFieldProps, "onChange">;
};

export type FieldFormButtonProps = Omit<ArnButtonProps, "onClick"> & {
  onClick: (record: any, navigate: any) => void;
};

export type ArnFormProps = {
  title: string;
  fields: Array<SelectFieldProps | InputFieldProps>;
  buttons: FieldFormButtonProps[];
};

export const ArnForm: FC<ArnFormProps> = ({ title, fields, buttons }) => {
  const [fieldRecords, setFieldRecords] = useState<FieldRecords>({});
  const buttonsWithHandlers = buttons.map((button) => ({
    ...button,
    onClick: button.onClick.bind(null, fieldRecords),
  }));

  return (
    <Container>
      <TitleContainer>
        <ArnTitle>{title}</ArnTitle>
      </TitleContainer>
      <FieldsContainer>
        {fields.map((field, i) => {
          switch (field.type) {
            case "input":
              return (
                <ArnInputField
                  {...field.props}
                  key={i}
                  onChange={(e) => {
                    const newValue = e.target.value;
                    fieldRecords[field.name] = newValue;
                    setFieldRecords(fieldRecords);
                  }}
                />
              );
            case "select":
              return (
                <ArnSelectField
                  {...field.props}
                  key={i}
                  onChange={(newOption) => {
                    if (newOption === null) {
                      fieldRecords[field.name] = "";
                    } else if (Array.isArray(newOption)) {
                      fieldRecords[field.name] = (newOption as any[]).map(
                        (option) => option.value
                      );
                    } else {
                      fieldRecords[field.name] = (newOption as any).value;
                    }

                    setFieldRecords(fieldRecords);
                  }}
                />
              );
            default:
              return null;
          }
        })}
      </FieldsContainer>
      <ArnButtonPanel buttons={buttonsWithHandlers} />
    </Container>
  );
};
