import styled from "styled-components";

export const Container = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  position: relative;
  width: 100%;
  height: 100%;
  flex: 1;
`;

export const TitleContainer = styled.div`
  font-weight: 600;
  font-size: 1.3rem;
  display: flex;
  position: relative;
  margin-bottom: 1rem;
`;

export const FieldsContainer = styled.div`
  display: flex;
  position: relative;
  flex-direction: column;
  flex: 1;
  margin-bottom: 1rem;
  & > :not(:last-child){
    margin-bottom: 1rem;
  }
`;
