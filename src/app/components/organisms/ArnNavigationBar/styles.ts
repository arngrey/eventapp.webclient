import styled from "styled-components";

export const ArnNavigationBarContainer = styled.div`
  display: flex;
  position: relative;
  flex-direction: row;
  justify-content: center;
  height: 4rem;
  width: 100%;
`;

export const ArnNavigationItemContainer = styled.div`
  display: flex;
  position: relative;
  height: 4rem;
  width: 9rem;
  border: 1px solid grey;
  border-radius: 0.3rem;
`;
