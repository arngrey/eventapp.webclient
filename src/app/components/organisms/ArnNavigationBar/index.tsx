import React, { FC } from "react";

import {
  ArnNavigationItem,
  ArnNavigationItemProps,
} from "app/components/molecules/ArnNavigationItem";

import {
  ArnNavigationBarContainer,
  ArnNavigationItemContainer,
} from "./styles";

export type ArnNavigationBarProps = {
  navigationItems: ArnNavigationItemProps[];
};

export const ArnNavigationBar: FC<ArnNavigationBarProps> = ({
  navigationItems,
}) => {
  return (
    <ArnNavigationBarContainer>
      {navigationItems
        .filter(
          (navigationItem) =>
            navigationItem.isVisible === undefined || navigationItem.isVisible
        )
        .map((navigationItem, i) => (
          <ArnNavigationItemContainer key={i}>
            <ArnNavigationItem key={i} {...navigationItem} />
          </ArnNavigationItemContainer>
        ))}
    </ArnNavigationBarContainer>
  );
};
