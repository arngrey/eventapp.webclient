import React, { FC } from "react";
import { Navigate, Outlet } from "react-router-dom";

export type ProtectedRouteProps = {
  authenticated: boolean;
  redirectPath?: string;
};

export const ProtectedRoute: FC<ProtectedRouteProps> = ({
  authenticated,
  redirectPath = "/welcome",
}) => (authenticated ? <Outlet /> : <Navigate to={redirectPath} replace />);
