import React, { FC, PropsWithChildren } from "react";

import { ArnHeaderContainer } from "./styles";

export const ArnHeader: FC<PropsWithChildren> = ({ children }) => {
  return <ArnHeaderContainer>{children}</ArnHeaderContainer>;
};
