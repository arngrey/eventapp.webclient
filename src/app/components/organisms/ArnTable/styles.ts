import styled from "styled-components";

export const Container = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  position: relative;
  flex: 1;
`;

export const TitleContainer = styled.div`
  font-weight: 600;
  font-size: 1.3rem;
  display: flex;
  position: relative;
  margin-bottom: 1rem;
`;

export const ButtonPanelContainer = styled.div`
  margin-bottom: 1rem;
`;

export const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
`

export interface ArnTableTheme {
  borderRadius: string,
  border: string;
  color: string;
  backgroundColor: string;
};

export const TABLE_THEME_DEFAULT: ArnTableTheme = {
  borderRadius: '.3rem',
  border: '1px solid lightgrey',
  color: 'black',
  backgroundColor: 'white',
}

export interface StyledArnTableProps {
  theme?: ArnTableTheme;
}

export const TableContainer = styled.div<StyledArnTableProps>`
  height: 100%;
  width: 100%;
  flex: 1;
  color: ${({ theme }) => theme.color};
  background-color: ${({ theme }) => theme.backgroundColor};
  border-radius: ${({ theme }) => theme.borderRadius};
  border: ${({ theme }) => theme.border};
`;