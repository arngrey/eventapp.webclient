import React, { FC, PropsWithChildren } from "react";

import { ArnBodyContainer } from "./styles";

export const ArnBody: FC<PropsWithChildren> = ({ children }) => {
  return <ArnBodyContainer>{children}</ArnBodyContainer>;
};
