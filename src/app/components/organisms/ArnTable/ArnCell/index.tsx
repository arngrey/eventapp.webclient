import React, { FC, PropsWithChildren } from "react";

import { DefaultArnCell } from "./styles";

type ArnCellProps = {};

export const ArnCell: FC<PropsWithChildren<ArnCellProps>> = ({ children }) => {
  return <DefaultArnCell>{children}</DefaultArnCell>;
};
