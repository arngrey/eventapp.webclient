import React, { FC, useContext } from "react";

import { ArnTitle } from "app/components/atoms/ArnTitle";
import {
  ArnButtonPanel,
  ArnButtonPanelProps,
} from "app/components/molecules/ArnButtonPanel";
import { ArnHeader } from "app/components/organisms/ArnTable/ArnHeader";
import { ArnRow } from "app/components/organisms/ArnTable/ArnRow";
import ThemeContext from "app/modules/common/ThemeContext";

import { ArnBody } from "./ArnBody";
import { ArnCell } from "./ArnCell";
import {
  Container,
  TitleContainer,
  TableContainer,
  Table,
  ButtonPanelContainer,
  StyledArnTableProps,
  TABLE_THEME_DEFAULT,
} from "./styles";

export type ArnTableProps = {
  title: string;
  columnNames: string[];
  rows: string[][];
  buttonPanel?: ArnButtonPanelProps;
  onRowClick?: (row: string[]) => void;
};

export const ArnTable: FC<ArnTableProps & StyledArnTableProps> = ({
  title,
  columnNames,
  rows,
  buttonPanel,
  onRowClick,
  theme,
}) => {
  const { table } = useContext(ThemeContext);
  return (
    <Container>
      <TitleContainer>
        <ArnTitle>{title}</ArnTitle>
      </TitleContainer>
      {buttonPanel && (
        <ButtonPanelContainer>
          <ArnButtonPanel buttons={buttonPanel.buttons} />
        </ButtonPanelContainer>
      )}
      <TableContainer theme={theme ?? table ?? TABLE_THEME_DEFAULT}>
        <Table>
          <ArnHeader>
            <ArnRow>
              {columnNames.map((columnName, i) => (
                <ArnCell key={i}>{columnName}</ArnCell>
              ))}
            </ArnRow>
          </ArnHeader>
          <ArnBody>
            {rows.map((row, i) => (
              <ArnRow
                key={i}
                onClick={
                  onRowClick !== undefined
                    ? () => {
                        onRowClick?.(row);
                      }
                    : undefined
                }
              >
                {row.map((value, i) => (
                  <ArnCell key={i}>{value}</ArnCell>
                ))}
              </ArnRow>
            ))}
          </ArnBody>
        </Table>
      </TableContainer>
    </Container>
  );
};
