import styled from "styled-components";

export interface ArnTableRowTheme {
    hoverBackgroundColor: string;
};

export const TABLE_ROW_THEME_DEFAULT: ArnTableRowTheme = {
    hoverBackgroundColor: '#b1b1b1',
}
 
export interface DefaultArnRowProps {
    selectable?: boolean;
    theme?: ArnTableRowTheme;
}

export const DefaultArnRow = styled.tr<DefaultArnRowProps>`
    ${
        ({ selectable = false, theme = TABLE_ROW_THEME_DEFAULT }) => 
            (selectable &&
                `&:hover {
                    background-color: ${theme.hoverBackgroundColor};
                    cursor: pointer;
                }`
            )
    }
`;
