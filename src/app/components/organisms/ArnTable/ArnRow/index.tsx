import React, { FC, PropsWithChildren, useContext } from "react";

import ThemeContext from "app/modules/common/ThemeContext";

import {
  DefaultArnRow,
  DefaultArnRowProps,
  TABLE_ROW_THEME_DEFAULT,
} from "./styles";

export type ArnRowProps = {
  onClick?: () => void;
};

export const ArnRow: FC<
  PropsWithChildren<ArnRowProps & DefaultArnRowProps>
> = ({ theme, onClick, children }) => {
  const { tableRow } = useContext(ThemeContext);
  return (
    <DefaultArnRow
      theme={theme ?? tableRow ?? TABLE_ROW_THEME_DEFAULT}
      selectable={onClick !== undefined}
      onClick={onClick}
    >
      {children}
    </DefaultArnRow>
  );
};
