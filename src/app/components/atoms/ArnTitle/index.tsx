import React, { FC, PropsWithChildren, useContext } from "react";

import ThemeContext from "app/modules/common/ThemeContext";

import {
  StyledArnTitle,
  StyledArnTitleProps,
  TITLE_THEME_DEFAULT,
} from "./styles";

interface ArnTitleProps extends StyledArnTitleProps {}

export const ArnTitle: FC<PropsWithChildren<ArnTitleProps>> = ({
  children,
  theme,
}) => {
  const { title } = useContext(ThemeContext);

  return (
    <StyledArnTitle theme={theme ?? title ?? TITLE_THEME_DEFAULT}>
      {children}
    </StyledArnTitle>
  );
};
