import styled from "styled-components";

export interface ArnTitleTheme {
  color: string;
};

export const TITLE_THEME_DEFAULT: ArnTitleTheme = {
  color: 'black',
}

export interface StyledArnTitleProps {
  theme?: ArnTitleTheme;
}

export const StyledArnTitle = styled.div<StyledArnTitleProps>`
  color: ${({ theme }) => theme.color};
`;