import React, { FC, PropsWithChildren } from "react";

import { ArnLayout } from "app/components/atoms/ArnLayout";

import { Background, Popup } from "./styles";

interface ArnPopupProps {
  isVisible: boolean;
}

export const ArnPopup: FC<PropsWithChildren<ArnPopupProps>> = ({
  isVisible,
  children,
}) => {
  if (!isVisible) {
    return null;
  }
  return (
    <>
      <Background />
      <Popup>
        <ArnLayout>{children}</ArnLayout>
      </Popup>
    </>
  );
};
