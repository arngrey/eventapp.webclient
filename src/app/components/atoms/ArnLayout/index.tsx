import React, { FC, PropsWithChildren, useContext } from "react";

import ThemeContext from "app/modules/common/ThemeContext";

import {
  StyledArnLayout,
  StyledArnLayoutProps,
  LAYOUT_THEME_DEFAULT,
} from "./styles";

interface ArnLayoutProps extends StyledArnLayoutProps {}

export const ArnLayout: FC<PropsWithChildren<ArnLayoutProps>> = ({
  theme,
  children,
}) => {
  const { layout } = useContext(ThemeContext);

  return (
    <StyledArnLayout theme={theme ?? layout ?? LAYOUT_THEME_DEFAULT}>
      {children}
    </StyledArnLayout>
  );
};
