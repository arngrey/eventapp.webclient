import styled from "styled-components";

export interface ArnLayoutTheme {
  borderRadius: string;
  backgroundColor: string;
  border: string;
};

export const LAYOUT_THEME_DEFAULT: ArnLayoutTheme = {
  borderRadius: '.3rem',
  backgroundColor: 'white',
  border: '1px solid black',
}

export interface StyledArnLayoutProps {
  theme?: ArnLayoutTheme;
}

export const StyledArnLayout = styled.div<StyledArnLayoutProps>`
  box-sizing: border-box;
  padding: 1rem 1.5rem;
  width: 100%;

  background-color: ${({ theme }) => theme.backgroundColor};
  border: ${({ theme }) => theme.border};
  border-radius: ${({ theme }) => theme.borderRadius};
`;
