import styled from "styled-components";

export interface ArnPageTheme {
  color: string;
  backgroundColor: string;
};

export const PAGE_THEME_DEFAULT: ArnPageTheme = {
  color: 'black',
  backgroundColor: 'white',
}

export interface StyledArnPageProps {
  theme?: ArnPageTheme;
}

export const StyledArnPage = styled.div<StyledArnPageProps>`
  box-sizing: border-box;

  width: 100%;
  height: 100%;

  color: ${({ theme }) => theme.color};
  background-color: ${({ theme }) => theme.backgroundColor};
`;
