import React, { FC, PropsWithChildren, useContext } from "react";

import ThemeContext from "app/modules/common/ThemeContext";

import {
  StyledArnPage,
  StyledArnPageProps,
  PAGE_THEME_DEFAULT,
} from "./styles";

interface ArnPageProps extends StyledArnPageProps {}

export const ArnPage: FC<PropsWithChildren<ArnPageProps>> = ({
  theme,
  children,
}) => {
  const { page } = useContext(ThemeContext);

  return (
    <StyledArnPage theme={theme ?? page ?? PAGE_THEME_DEFAULT}>
      {children}
    </StyledArnPage>
  );
};
