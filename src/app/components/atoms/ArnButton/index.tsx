import React, { FC, useContext } from "react";
import { useNavigate } from "react-router-dom";

import ThemeContext from "app/modules/common/ThemeContext";

import {
  BUTTON_THEME_DEFAULT,
  StyledArnButton,
  StyledArnButtonProps,
} from "./styles";

export interface ArnButtonProps extends StyledArnButtonProps {
  text: string;
  onClick: (navigate: any) => void;
  visible?: boolean;
}

export const ArnButton: FC<ArnButtonProps> = ({
  text,
  onClick,
  theme,
  visible = true,
}) => {
  const navigate = useNavigate();
  const { button } = useContext(ThemeContext);

  if (!visible) {
    return null;
  }

  return (
    <StyledArnButton
      theme={theme ?? button ?? BUTTON_THEME_DEFAULT}
      onClick={() => {
        onClick(navigate);
      }}
    >
      {text}
    </StyledArnButton>
  );
};
