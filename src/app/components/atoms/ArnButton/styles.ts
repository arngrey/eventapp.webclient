import styled from "styled-components";

export enum ButtonKind {
  Default = "Default",
  Primary = "Primary",
}

interface ArnButtonKindTheme {
  borderRadius: string;
  color: string;
  backgroundColor: string;
  border: string;
  activeColor: string;
  activeBackgroundColor: string;
  activeBorder: string;
  hoverColor: string;
  hoverBackgroundColor: string;
  hoverBorder: string;
  focusColor: string;
  focusBackgroundColor: string;
  focusBorder: string;
  focusOutline: string;
};

export type ArnButtonTheme = Record<ButtonKind, ArnButtonKindTheme>;

export interface StyledArnButtonProps {
  kind?: ButtonKind;
  theme?: ArnButtonTheme;
}

export const BUTTON_THEME_DEFAULT: ArnButtonTheme = {
  [ButtonKind.Default]: {
    borderRadius: '4px',
    color: 'black',
    backgroundColor: 'white',
    border: '1px solid grey',
    activeColor: 'black',
    activeBackgroundColor: '#b1b1b1',
    activeBorder: '1px solid grey',
    hoverColor: 'black',
    hoverBackgroundColor: '#b1b1b1',
    hoverBorder: '1px solid grey',
    focusColor: 'black',
    focusBackgroundColor: 'white',
    focusBorder: '1px solid grey',
    focusOutline: 'none',  
  },
  [ButtonKind.Primary]: {
    borderRadius: '4px',
    color: 'black',
    backgroundColor: 'white',
    border: '1px solid grey',
    activeColor: 'black',
    activeBackgroundColor: '#b1b1b1',
    activeBorder: '1px solid grey',
    hoverColor: 'black',
    hoverBackgroundColor: '#b1b1b1',
    hoverBorder: '1px solid grey',
    focusColor: 'black',
    focusBackgroundColor: 'white',
    focusBorder: '1px solid grey',
    focusOutline: 'none',  
  }
}

export const StyledArnButton = styled.button<StyledArnButtonProps>`
  padding: 8px 16px;
  box-sizing: border-box;

  border-radius: ${({ theme, kind = ButtonKind.Default }) => theme[kind].borderRadius};
  color: ${({ theme, kind = ButtonKind.Default }) => theme[kind].color};
  background-color: ${({ theme, kind = ButtonKind.Default }) => theme[kind].backgroundColor};
  border: ${({ theme, kind = ButtonKind.Default }) => theme[kind].border};
  &:active {
    color: ${({ theme, kind = ButtonKind.Default }) => theme[kind].activeColor};
    background-color: ${({ theme, kind = ButtonKind.Default }) => theme[kind].activeBackgroundColor};
    border: ${({ theme, kind = ButtonKind.Default }) => theme[kind].activeBorder};
  }
  &:hover {
    color: ${({ theme, kind = ButtonKind.Default }) => theme[kind].hoverColor};
    background-color: ${({ theme, kind = ButtonKind.Default }) => theme[kind].hoverBackgroundColor};
    border: ${({ theme, kind = ButtonKind.Default }) => theme[kind].hoverBorder};
  }
  &:focus {
    color: ${({ theme, kind = ButtonKind.Default }) => theme[kind].focusColor};
    background-color: ${({ theme, kind = ButtonKind.Default }) => theme[kind].focusBackgroundColor};
    border: ${({ theme, kind = ButtonKind.Default }) => theme[kind].focusBorder};
    outline: ${({ theme, kind = ButtonKind.Default }) => theme[kind].focusOutline};
  }
`;
