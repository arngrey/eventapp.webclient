import styled from "styled-components";

export interface ArnInputTheme {
  borderRadius: string;
  color: string;
  backgroundColor: string;
  border: string;
  borderBottom: string;
  focusOutline: string;
  focusBorderBottom: string;
};

export const INPUT_THEME_DEFAULT: ArnInputTheme = {
  borderRadius: '0',
  color: 'black',
  backgroundColor: 'transparent',
  border: 'none',
  borderBottom: '.1rem solid grey',
  focusOutline: 'none',
  focusBorderBottom: '.2rem solid black',
}

export interface StyledArnInputProps {
  theme?: ArnInputTheme;
  stretched?: boolean;
}

export const StyledArnInput = styled.input<StyledArnInputProps>`
  position: relative;
  width: ${({ stretched = false }) => stretched ? '100%' : '300px'};
  height: 2rem;
  box-sizing: border-box;
  
  border-radius: ${({ theme }) => theme.borderRadius};
  color: ${({ theme }) => theme.color};
  background-color: ${({ theme }) => theme.backgroundColor};
  border: ${({ theme }) => theme.border};
  border-bottom: ${({ theme }) => theme.borderBottom};
  &:focus {
    outline: ${({ theme }) => theme.focusOutline};
    border-bottom: ${({ theme }) => theme.focusBorderBottom};
  }
`;
