import React, { ChangeEvent, FC, useContext } from "react";

import ThemeContext from "app/modules/common/ThemeContext";

import {
  StyledArnInput,
  StyledArnInputProps,
  INPUT_THEME_DEFAULT,
} from "./styles";

interface ArnInputProps extends StyledArnInputProps {
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

export const ArnInput: FC<ArnInputProps> = ({ onChange, theme, stretched }) => {
  const { input } = useContext(ThemeContext);

  return (
    <StyledArnInput
      onChange={onChange}
      theme={theme ?? input ?? INPUT_THEME_DEFAULT}
      stretched={stretched}
    />
  );
};
