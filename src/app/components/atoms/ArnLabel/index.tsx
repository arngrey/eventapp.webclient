import React, { FC, useContext } from "react";

import ThemeContext from "app/modules/common/ThemeContext";

import {
  StyledArnLabel,
  StyledArnLabelProps,
  LABEL_THEME_DEFAULT,
} from "./styles";

interface ArnLabelProps extends StyledArnLabelProps {
  text: string;
}

export const ArnLabel: FC<ArnLabelProps> = ({ text, theme }) => {
  const { label } = useContext(ThemeContext);

  return (
    <StyledArnLabel theme={theme ?? label ?? LABEL_THEME_DEFAULT}>
      {text}
    </StyledArnLabel>
  );
};
