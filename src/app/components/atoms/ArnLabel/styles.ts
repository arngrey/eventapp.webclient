import styled from "styled-components";

export interface ArnLabelTheme {
  color: string;
};

export const LABEL_THEME_DEFAULT: ArnLabelTheme = {
  color: 'black',
}

export interface StyledArnLabelProps {
  theme?: ArnLabelTheme;
}

export const StyledArnLabel = styled.label<StyledArnLabelProps>`
  color: ${({ theme }) => theme.color};
`;