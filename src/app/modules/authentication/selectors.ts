import { AuthenticationState } from "app/models/authentication";
import { RootState } from "app/store";

export const selectAuthentication: (state: RootState) => AuthenticationState = (
  state
) => state.authentication;
