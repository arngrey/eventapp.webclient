import { createContext } from "react";

import { ArnButtonTheme } from "app/components/atoms/ArnButton/styles";
import { ArnInputTheme } from "app/components/atoms/ArnInput/styles";
import { ArnLabelTheme } from "app/components/atoms/ArnLabel/styles";
import { ArnLayoutTheme } from "app/components/atoms/ArnLayout/styles";
import { ArnPageTheme } from "app/components/atoms/ArnPage/styles";
import { ArnTitleTheme } from "app/components/atoms/ArnTitle/styles";
import { ArnTableRowTheme } from "app/components/organisms/ArnTable/ArnRow/styles";
import { ArnTableTheme } from "app/components/organisms/ArnTable/styles";

import { THEME_DEFAULT } from "./theme/default";

// TODO: use ThemeContext from styled-components

export interface ArnTheme {
  background: string;
  surface: string;
  primary: string;
  secondary: string;
  onBackground: string;
  onSurface: string;
  onPrimary: string;
  onSecondary: string;
  button: ArnButtonTheme;
  input: ArnInputTheme;
  label: ArnLabelTheme;
  layout: ArnLayoutTheme;
  title: ArnTitleTheme;
  page: ArnPageTheme;
  table: ArnTableTheme;
  tableRow: ArnTableRowTheme;
}


export default createContext(THEME_DEFAULT);