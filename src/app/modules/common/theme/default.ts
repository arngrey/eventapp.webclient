import { BUTTON_THEME_DEFAULT } from "app/components/atoms/ArnButton/styles";
import { INPUT_THEME_DEFAULT } from "app/components/atoms/ArnInput/styles";
import { LABEL_THEME_DEFAULT } from "app/components/atoms/ArnLabel/styles";
import { LAYOUT_THEME_DEFAULT } from "app/components/atoms/ArnLayout/styles";
import { PAGE_THEME_DEFAULT } from "app/components/atoms/ArnPage/styles";
import { TITLE_THEME_DEFAULT } from "app/components/atoms/ArnTitle/styles";
import { TABLE_ROW_THEME_DEFAULT } from "app/components/organisms/ArnTable/ArnRow/styles";
import { TABLE_THEME_DEFAULT } from "app/components/organisms/ArnTable/styles";
import { ArnTheme } from "app/modules/common/ThemeContext";

export const THEME_DEFAULT: ArnTheme = {
  background: '',
  surface: '',
  primary: '',
  secondary: '',
  onBackground: '',
  onSurface: '',
  onPrimary: '',
  onSecondary: '',
  button: BUTTON_THEME_DEFAULT,
  input: INPUT_THEME_DEFAULT,
  label: LABEL_THEME_DEFAULT,
  layout: LAYOUT_THEME_DEFAULT,
  title: TITLE_THEME_DEFAULT,
  page: PAGE_THEME_DEFAULT,
  table: TABLE_THEME_DEFAULT,
  tableRow: TABLE_ROW_THEME_DEFAULT,
}