import { ButtonKind } from "app/components/atoms/ArnButton/styles";
import { ArnTheme } from "app/modules/common/ThemeContext";

// target = opacity * overlay + (1 - opacity) * background
// ??? = 255 * 0.1 + 0.9 * background color

const COLOR_TRANSPARENT = 'transparent';
const COLOR_BACKGROUND = 'rgb(18,18,18)';
const COLOR_SURFACE = 'rgb(34,34,34)';
const COLOR_SURFACE_ACTIVE = 'rgb(56,56,56)';
const COLOR_SURFACE_HOVER = 'rgb(43,43,43)';
const COLOR_SURFACE_FOCUSED = 'rgb(61,61,61)';
const COLOR_PRIMARY = 'rgb(187,134,252)';
const COLOR_PRIMARY_ACTIVE = 'rgb(194,146,252)';
const COLOR_PRIMARY_HOVER = 'rgb(190,139,252)';
const COLOR_PRIMARY_FOCUSED = 'rgb(198,149,252)';
const COLOR_SECONDARY = '#03DAC6';
const COLOR_ON_BACKGROUND = '#FFFFFF';
const COLOR_ON_SURFACE = '#FFFFFF';
const COLOR_ON_PRIMARY = '#000000';
const COLOR_ON_SECONDARY = '#000000';

const THEME_DARK: ArnTheme = {
  background: COLOR_BACKGROUND,
  surface: COLOR_SURFACE,
  primary: COLOR_PRIMARY,
  secondary: COLOR_SECONDARY,
  onBackground: COLOR_ON_BACKGROUND,
  onSurface: COLOR_ON_SURFACE,
  onPrimary: COLOR_ON_PRIMARY,
  onSecondary: COLOR_ON_SECONDARY,
  button: {
    [ButtonKind.Primary]: {
      borderRadius: '.3rem',
      color: COLOR_ON_PRIMARY,
      backgroundColor: COLOR_PRIMARY,
      border: 'none',
      activeColor: COLOR_ON_PRIMARY,
      activeBackgroundColor: COLOR_PRIMARY_ACTIVE,
      activeBorder: 'none',
      hoverColor: COLOR_ON_PRIMARY,
      hoverBackgroundColor: COLOR_PRIMARY_HOVER,
      hoverBorder: 'none',
      focusColor: COLOR_ON_PRIMARY,
      focusBackgroundColor: COLOR_PRIMARY_FOCUSED,
      focusBorder: 'none',
      focusOutline: 'none',
    },
    [ButtonKind.Default]: {
      borderRadius: '.3rem',
      color: COLOR_ON_SURFACE,
      backgroundColor: COLOR_SURFACE,
      border: '1px solid ' + COLOR_ON_SURFACE,
      activeColor: COLOR_ON_SURFACE,
      activeBackgroundColor: COLOR_SURFACE_ACTIVE,
      activeBorder: '1px solid ' + COLOR_ON_SURFACE,
      hoverColor: COLOR_ON_SURFACE,
      hoverBackgroundColor: COLOR_SURFACE_HOVER,
      hoverBorder: '1px solid ' + COLOR_ON_SURFACE,
      focusColor: COLOR_ON_SURFACE,
      focusBackgroundColor: COLOR_SURFACE_FOCUSED,
      focusBorder: '1px solid ' + COLOR_TRANSPARENT,
      focusOutline: '.2rem solid ' + COLOR_PRIMARY,
    },
  },
  input: {
    borderRadius: '0',
    color: COLOR_ON_SURFACE,
    backgroundColor: 'transparent',
    border: 'none',
    borderBottom: '.1rem solid grey',
    focusOutline: 'none',
    focusBorderBottom: '.2rem solid ' + COLOR_PRIMARY,
  },
  label: {
    color: COLOR_ON_SURFACE,
  },
  layout: {
    borderRadius: '.3rem',
    backgroundColor: COLOR_SURFACE,
    border: 'none',
  },
  title: {
    color: COLOR_PRIMARY,
  },
  page: {
    color: COLOR_ON_BACKGROUND,
    backgroundColor: COLOR_BACKGROUND,
  },
  table: {
    border: 'none',
    borderRadius: '.3rem',
    color: COLOR_ON_SURFACE,
    backgroundColor: COLOR_SURFACE,
  },
  tableRow: {
    hoverBackgroundColor: COLOR_SURFACE_HOVER,
  }
}

export default THEME_DARK;
