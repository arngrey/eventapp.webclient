import styled from "styled-components";

export const RegisterContainer = styled.div`
  box-sizing: border-box;
  height: 100%;
  width: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 1rem;
`;

export const CardContainer = styled.div`
  box-sizing: border-box;
  position: relative;
  display: flex;
  flex-direction: column;
  min-width: 70rem;
`;
