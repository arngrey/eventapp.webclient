import React, { FC, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

import { ArnPopup } from "app/components/atoms/ArnPopup";
import { ArnForm } from "app/components/organisms/ArnForm";
import { ArnTable } from "app/components/organisms/ArnTable";
import { RegisterContainer } from "app/containers/styles";
import { useAppDispatch, useAppSelector } from "app/hooks";
import { selectHobbies } from "app/selectors";
import { loadHobbiesAsync, createHobbyAsync } from "app/slice";

export const HobbyRegister: FC = () => {
  const dispatch = useAppDispatch();

  const location = useLocation();
  useEffect(() => {
    dispatch(loadHobbiesAsync());
  }, [dispatch, location]);

  const hobbies = useAppSelector(selectHobbies);
  const [isPopupVisible, setPopupVisibility] = useState(false);

  return (
    <RegisterContainer>
      <ArnTable
        title="Список хобби"
        columnNames={["Идентификатор", "Наименование"]}
        rows={hobbies.map(({ id, name }) => [id, name])}
        buttonPanel={{
          buttons: [
            {
              text: "Добавить",
              onClick: () => {
                setPopupVisibility(true);
              },
            },
          ],
        }}
      />
      <ArnPopup isVisible={isPopupVisible}>
        <ArnForm
          title={"Создание хобби"}
          fields={[
            {
              name: "name",
              type: "input",
              props: { labelText: "Наименование" },
            },
          ]}
          buttons={[
            {
              text: "Создать",
              onClick: async (records) => {
                await dispatch(createHobbyAsync(records["name"]));
                await dispatch(loadHobbiesAsync());
                setPopupVisibility(false);
              },
            },
            {
              text: "Отмена",
              onClick: async () => {
                setPopupVisibility(false);
              },
            },
          ]}
        />
      </ArnPopup>
    </RegisterContainer>
  );
};
