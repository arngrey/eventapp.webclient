import React, { FC, useState } from "react";

import { ArnPopup } from "app/components/atoms/ArnPopup";
import { ArnTitle } from "app/components/atoms/ArnTitle";
import { ArnButtonPanel } from "app/components/molecules/ArnButtonPanel";
import { ArnForm } from "app/components/organisms/ArnForm";
import { ArnTable } from "app/components/organisms/ArnTable";
import { CardContainer } from "app/containers/styles";
import { useAppDispatch, useAppSelector } from "app/hooks";
import { selectAuthentication } from "app/modules/authentication/selectors";
import { selectCampaign } from "app/selectors";
import {
  loadCampaignsAsync,
  loadCampaignMessagesAsync,
  sendMessageAsync,
  addParticipantAsync,
} from "app/slice";

import {
  CommonButtonPanelContainer,
  TableContainer,
  CardTitleContainer,
} from "./styles";

export type CampaignCardProps = {
  campaignId: string;
  onClose: () => void;
};

export const CampaignCard: FC<CampaignCardProps> = ({
  campaignId,
  onClose,
}) => {
  const dispatch = useAppDispatch();
  const authentication = useAppSelector(selectAuthentication);
  const campaign = useAppSelector(selectCampaign(campaignId));
  const [isAddingMessagePopupVisible, setAddingMessagePopupVisibility] =
    useState(false);

  if (!authentication.isAuthenticated) {
    return null;
  }

  if (campaign === undefined) {
    return null;
  }

  const { isAuthenticated, userId } = authentication;
  const campaignMessages =
    campaign.messages === undefined ? [] : campaign.messages;

  return (
    <CardContainer>
      <CardTitleContainer>
        <ArnTitle>{`Кампания ${campaign.name}`}</ArnTitle>
      </CardTitleContainer>
      <TableContainer>
        <ArnTable
          title="Список сообщений кампании"
          columnNames={["Идентификатор", "Отправитель", "Текст"]}
          rows={campaignMessages.map(({ id, sender, text }) => [
            id,
            sender.login,
            text,
          ])}
          buttonPanel={{
            buttons: [
              {
                text: "Обновить",
                onClick: () => {
                  dispatch(loadCampaignMessagesAsync(campaignId));
                },
              },
              {
                text: "Создать сообщение",
                onClick: () => {
                  setAddingMessagePopupVisibility(true);
                },
              },
            ],
          }}
        />
      </TableContainer>
      <CommonButtonPanelContainer>
        <ArnButtonPanel
          buttons={[
            {
              text: "Закрыть",
              onClick: () => {
                onClose();
              },
            },
            {
              text: "Присоединиться",
              onClick: async () => {
                await dispatch(addParticipantAsync(userId, campaignId));
                await dispatch(loadCampaignsAsync());
                await dispatch(loadCampaignMessagesAsync(campaignId));
              },
              visible: !campaign.participants.some(({ id }) => id === userId),
            },
          ]}
        />
      </CommonButtonPanelContainer>
      <ArnPopup isVisible={isAddingMessagePopupVisible}>
        <ArnForm
          title="Отправить сообщение"
          fields={[
            { name: "text", type: "input", props: { labelText: "Текст" } },
          ]}
          buttons={[
            {
              text: "Отправить",
              onClick: async (records) => {
                if (!isAuthenticated) {
                  return;
                }
                await dispatch(
                  sendMessageAsync(userId, campaignId, records["text"])
                );
                await dispatch(loadCampaignMessagesAsync(campaignId));
                setAddingMessagePopupVisibility(false);
              },
            },
            {
              text: "Отмена",
              onClick: async (records) => {
                setAddingMessagePopupVisibility(false);
              },
            },
          ]}
        />
      </ArnPopup>
    </CardContainer>
  );
};
