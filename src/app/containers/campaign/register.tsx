import React, { FC, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

import { ArnPopup } from "app/components/atoms/ArnPopup";
import { ArnForm } from "app/components/organisms/ArnForm";
import { ArnTable } from "app/components/organisms/ArnTable";
import { RegisterContainer } from "app/containers/styles";
import { useAppDispatch, useAppSelector } from "app/hooks";
import { selectAuthentication } from "app/modules/authentication/selectors";
import { selectCampaigns, selectHobbies } from "app/selectors";
import {
  loadCampaignsAsync,
  loadCampaignMessagesAsync,
  createCampaignAsync,
} from "app/slice";

import { CampaignCard } from "./Card";

export const CampaignRegister: FC = () => {
  const dispatch = useAppDispatch();

  const location = useLocation();
  useEffect(() => {
    dispatch(loadCampaignsAsync());
  }, [dispatch, location]);

  const authentication = useAppSelector(selectAuthentication);
  const campaigns = useAppSelector(selectCampaigns);
  const hobbies = useAppSelector(selectHobbies);
  const [campaignId, setCampaignId] = useState("");
  const [isAddingCampaignPopupVisible, setAddingCampaignPopupVisibility] =
    useState(false);
  const [isCardPopupVisible, setCardPopupVisibility] = useState(false);

  if (!authentication.isAuthenticated) {
    return null;
  }

  return (
    <RegisterContainer>
      <ArnTable
        title="Список кампаний"
        columnNames={["Идентификатор", "Наименование", "Участники", "Хобби"]}
        rows={campaigns.map(({ id, name, participants, hobbies }) => [
          id,
          name,
          participants.map(({ login }) => login).join(", "),
          hobbies.map(({ name }) => name).join(", "),
        ])}
        buttonPanel={{
          buttons: [
            {
              text: "Добавить кампанию",
              onClick: () => {
                setAddingCampaignPopupVisibility(true);
              },
            },
          ],
        }}
        onRowClick={(row) => {
          setCampaignId(row[0]);
          setCardPopupVisibility(true);
          dispatch(loadCampaignMessagesAsync(row[0]));
        }}
      />
      <ArnPopup isVisible={isAddingCampaignPopupVisible}>
        <ArnForm
          title="Создание кампании"
          fields={[
            {
              name: "name",
              type: "input",
              props: { labelText: "Наименование" },
            },
            {
              name: "hobbyIds",
              type: "select",
              props: {
                labelText: "Идентификаторы хобби",
                options: hobbies.map(({ id, name }) => ({
                  value: id,
                  label: name,
                })),
                isMultiple: true,
              },
            },
          ]}
          buttons={[
            {
              text: "Создать",
              onClick: async (records) => {
                await dispatch(
                  createCampaignAsync(
                    records["name"],
                    authentication.userId,
                    records["hobbyIds"]
                  )
                );
                await dispatch(loadCampaignsAsync());
                setAddingCampaignPopupVisibility(false);
              },
            },
            {
              text: "Отмена",
              onClick: async () => {
                setAddingCampaignPopupVisibility(false);
              },
            },
          ]}
        />
      </ArnPopup>
      <ArnPopup isVisible={isCardPopupVisible}>
        <CampaignCard
          campaignId={campaignId}
          onClose={() => {
            setCardPopupVisibility(false);
          }}
        />
      </ArnPopup>
    </RegisterContainer>
  );
};
