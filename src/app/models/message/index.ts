import { CampaignFlatDto } from "app/models/campaign";
import { UserFlatDto } from "app/models/user";

export interface MessageDto {
  id: string;
  sender: UserFlatDto;
  campaign: CampaignFlatDto;
  text: string;
  created: Date;
}

export interface MessageFlatDto {
  id: string;
  senderId: string;
  campaignId: string;
  text: string;
  created: Date;
}
