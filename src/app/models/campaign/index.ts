import { HobbyFlatDto } from "app/models/hobby";
import { MessageDto } from "app/models/message";
import { UserFlatDto } from "app/models/user";

export interface CampaignDto {
  id: string;
  name: string;
  administrator: UserFlatDto;
  hobbies: HobbyFlatDto[];
  participants: UserFlatDto[];
  messages?: MessageDto[];
}

export interface CampaignFlatDto {
  id: string;
  name: string;
  administratorId: string;
  hobbyIds: string[];
  participantIds: string[];
  messageIds: string[];
}
