import React, { FC } from "react";

import { ArnButtonPanel } from "app/components/molecules/ArnButtonPanel";
import { ArnNavigationBar } from "app/components/organisms/ArnNavigationBar";
import { useAppDispatch } from "app/hooks";
import { logOutAsync } from "app/modules/authentication/slice";

const MainPage: FC = () => {
  const dispatch = useAppDispatch();
  return (
    <>
      <ArnNavigationBar
        navigationItems={[
          { title: "Главная", path: "/main" },
          { title: "Хобби", path: "/hobbies" },
          { title: "Кампании", path: "/campaigns" },
        ]}
      />
      <ArnButtonPanel
        buttons={[
          {
            text: "Выйти",
            onClick: async (navigate) => {
              await dispatch(logOutAsync());
              navigate("/welcome");
            },
          },
        ]}
      />
    </>
  );
};

export default MainPage;
