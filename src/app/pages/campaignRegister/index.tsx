import React, { FC } from "react";

import { ArnPage } from "app/components/atoms/ArnPage";
import { CampaignRegister } from "app/containers/campaign/register";

const CampaignRegisterPage: FC = () => {
  return (
    <ArnPage>
      <CampaignRegister />
    </ArnPage>
  );
};

export default CampaignRegisterPage;
