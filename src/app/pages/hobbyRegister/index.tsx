import React, { FC } from "react";

import { ArnPage } from "app/components/atoms/ArnPage";
import { HobbyRegister } from "app/containers/hobby/register";

const HobbyRegisterPage: FC = () => {
  return (
    <ArnPage>
      <HobbyRegister />
    </ArnPage>
  );
};

export default HobbyRegisterPage;
