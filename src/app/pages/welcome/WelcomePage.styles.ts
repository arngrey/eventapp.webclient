import styled from "styled-components";

interface ContainerProps {
  backgroundColor: string;
}

export const Container = styled.div<ContainerProps>`
  height: 100%;
  width: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
  background-color: ${({ backgroundColor }) => backgroundColor};
  align-items: center;
`;

export const TitleContainer = styled.div`
  position: relative;
  display: flex;
  color: white;
  font-size: 5rem;
  font-weight: 500;
  margin-top: 10rem;
`;

export const FormContainer = styled.div`
  position: relative;
  display: flex;
  width: 20rem;
  margin-top: 7rem;
`;
