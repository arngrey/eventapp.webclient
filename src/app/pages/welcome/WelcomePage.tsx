import React, { useContext } from "react";

import { ArnLayout } from "app/components/atoms/ArnLayout";
import { ArnTitle } from "app/components/atoms/ArnTitle";
import { ArnForm } from "app/components/organisms/ArnForm";
import { useAppDispatch } from "app/hooks";
import { signInAsync, signUpAsync } from "app/modules/authentication/slice";
import ThemeContext from "app/modules/common/ThemeContext";

import { FormContainer, TitleContainer, Container } from "./WelcomePage.styles";

function WelcomePage() {
  const dispatch = useAppDispatch();
  const { background, onBackground } = useContext(ThemeContext);

  return (
    <Container backgroundColor={background}>
      <TitleContainer>
        <ArnTitle theme={{ color: onBackground }}>Campaign Finder</ArnTitle>
      </TitleContainer>
      <FormContainer>
        <ArnLayout>
          <ArnForm
            title="Войти"
            fields={[
              { name: "login", type: "input", props: { labelText: "Логин" } },
              {
                name: "password",
                type: "input",
                props: { labelText: "Пароль" },
              },
            ]}
            buttons={[
              {
                text: "Войти",
                onClick: async (record, navigate) => {
                  await dispatch(
                    signInAsync(record["login"], record["password"])
                  );
                  navigate("/main");
                },
              },
              {
                text: "Зарегистрироваться",
                onClick: async (record, navigate) => {
                  await dispatch(
                    signUpAsync(record["login"], record["password"])
                  );
                  navigate("/main");
                },
              },
            ]}
          />
        </ArnLayout>
      </FormContainer>
    </Container>
  );
}

export default WelcomePage;
