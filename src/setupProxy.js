const path = require('path');

const apiMocker = require('mocker-api');

module.exports = function(app) {
  if (process.env.NODE_ENV === 'production') {
    return;
  }

  apiMocker(app, path.resolve('./mocker/index.js'), {
    changeHost: true
  });
};