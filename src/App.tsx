import React from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";

import "./App.css";
import CampaignRegisterPage from "app/pages/campaignRegister";
import HobbyRegisterPage from "app/pages/hobbyRegister";

import { ProtectedRoute } from "./app/components/organisms/ProtectedRoute";
import { useAppSelector } from "./app/hooks";
import { selectAuthentication } from "./app/modules/authentication/selectors";
import MainPage from "./app/pages/main";
import WelcomePage from "./app/pages/welcome/WelcomePage";
import { AppContainer } from "./App.styles";

function App() {
  const { isAuthenticated } = useAppSelector(selectAuthentication);

  return (
    <AppContainer>
      <BrowserRouter>
        <Routes>
          <Route path="/welcome" element={<WelcomePage />} />
          <Route
            path="/"
            element={<ProtectedRoute authenticated={isAuthenticated} />}
          >
            <Route path="/hobbies" element={<HobbyRegisterPage />} />
            <Route path="/campaigns" element={<CampaignRegisterPage />} />
            <Route path="/main" element={<MainPage />} />
          </Route>
          <Route
            path="*"
            element={<Navigate to={{ pathname: "/welcome" }} />}
          />
        </Routes>
      </BrowserRouter>
    </AppContainer>
  );
}

export default App;
