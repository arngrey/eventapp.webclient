#!/bin/bash

# Run nginx
# When main process is finished, container stops
# Nginx starts as daemon by default
# Disable deamonization to prevent container stopping
nginx -g 'daemon off;' &

consul agent \
  -bind $PRIVATE_IP_ADDRESS \
  -advertise $PRIVATE_IP_ADDRESS \
  -join consul_server \
  -node $NODE \
  -dns-port 53 \
  -data-dir /data \
  -config-dir /etc/consul.d \
  -enable-local-script-checks `# Healthchecks. Enable script checks defined in local config files. Script checks defined via the HTTP API will not be allowed.`